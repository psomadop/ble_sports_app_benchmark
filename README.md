# README #

Having the Nordik SDKv5_1 installed, and uVision 5 (that's what I used), checkout the source in a separate directory under
<Nordik SDK location>\Nordic\nrf51822\Board\nrf6310\s110

Open the uVision project file from the <app>/arm folder and build. 

Connect to it with a GATTS Server, select the characteristic with UUID 4444 and enable notifications. The first two octets are the data rate achieved, expressed in bytes per second.

The application triggers the transfer as soon as the server enables notifications. We queue up as many notifications as possible until the chip buffer is full. We continue notifications when the chip says there is some buffer space.

A notification can be triggered in many different ways, but this is what I thought would allow us to measure the maximum realistic data transfer rate, using the BLE Host Stack facilities.

### What is this repository for? ###

This is my private repository to track the changes I've done to John's repository.

### Who do I talk to? ###

psomadop@gmail.com