#include "ble_sports_app_service.h"
#include <string.h>
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "nrf_gpio.h"
#include "nrf51_bitfields.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "app_scheduler.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "ble_error_log.h"
#include "app_gpiote.h"
#include "app_button.h"
#include "ble_debug_assert_handler.h"
#include "pstorage.h"

extern uint32_t octets_sent;
extern unsigned char MMG_measurement[];

uint32_t ble_sports_app_send_MMG(ble_sas_t * vsas, unsigned char * value)
{
		ble_gatts_hvx_params_t 					params;
		uint16_t len 										= 20;
		value[19]												=	0x04;
		
		/* 
			Keep some handy statistics. Increment the total number
			of octets we are about to send off-chip. Increment a serial 
			number in the UUID value to let the server know we have
			a fresh MMG measurement. This is useless but I love it for
			debugging purposes.
	
			Also toggle LED_0 to indicate this activity. Lets me know
			the app is still alive before it crashes.
		*/
		octets_sent+=20;
		MMG_measurement[18]++;
		nrf_gpio_pin_toggle(LED_0);
	
		memset(&params, 0, sizeof(params));
		params.type 										= BLE_GATT_HVX_NOTIFICATION;
		params.handle 									= vsas->mmg_handles.value_handle;
		params.p_data 									= value;
		params.p_len 										= &len;
		return sd_ble_gatts_hvx(vsas->conn_handle, &params);
}

static uint32_t mmg_characteristic_add(ble_sas_t * p_sas)
{
    ble_gatts_char_md_t 						char_md;
    ble_gatts_attr_md_t 						cccd_md;
    ble_gatts_attr_t    						attr_char_value;
    ble_uuid_t          						ble_uuid;
    ble_gatts_attr_md_t 						attr_md;
    
    memset(&cccd_md, 0, sizeof(cccd_md));
    // According to BAS_SPEC_V10, the read operation on cccd should be possible without
    // authentication.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read   = 1;
    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_sas->uuid_type;
		ble_uuid.uuid = SPORTS_APP_UUID_MMG_CHAR;
		
    memset(&attr_md, 0, sizeof(attr_md));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 20;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 20;//sizeof(uint8_t);
    attr_char_value.p_value      = NULL;
    
		return sd_ble_gatts_characteristic_add(p_sas->service_handle, &char_md, &attr_char_value, &p_sas->mmg_handles);
    
}

uint32_t ble_sas_init(ble_sas_t * p_sas)
{
    uint32_t   							err_code;
    ble_uuid_t 							ble_uuid;
    
    // Add service
		ble_uuid128_t 										base_uuid = SPORTS_APP_UUID_BASE;
		err_code 													= sd_ble_uuid_vs_add(&base_uuid, &p_sas->uuid_type);
		if (err_code != NRF_SUCCESS)
		{
				return err_code;
		}
		
		ble_uuid.type = p_sas->uuid_type;
		ble_uuid.uuid = SPORTS_APP_UUID_SERVICE;
		err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_sas->service_handle);
		if (err_code != NRF_SUCCESS)
		{
				return err_code;
		}
    // Add characteristics
		err_code = mmg_characteristic_add(p_sas);
		if (err_code != NRF_SUCCESS)
		{
				return err_code;
		}
		return NRF_SUCCESS;
    
}
