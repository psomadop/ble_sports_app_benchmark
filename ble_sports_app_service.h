#ifndef BLE_SAS_H__
#define BLE_SAS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

//2916xxxx-2E50-42B8-3B40-7327BFA10D74
#define SPORTS_APP_UUID_BASE {0x74, 0x0D, 0xA1, 0xBF, 0x27, 0x73, 0x40, 0x3B, 0xB8, 0x42, 0x50, 0x2E, 0x00, 0x00, 0x16, 0x29}
#define SPORTS_APP_UUID_SERVICE 0x4444
#define SPORTS_APP_UUID_MMG_CHAR 0x4441
//#define SPORTS_APP_UUID_LED_CHAR 0x4442

/**@brief Battery Service structure. This contains various status information for the service. */
typedef struct
{
    uint16_t 																					service_handle;
		ble_gatts_char_handles_t 													mmg_handles;
		uint16_t 																					conn_handle;
		uint8_t 																					uuid_type;
} ble_sas_t;

/**@brief Function for initializing the Battery Service.
 *
 * @param[out]  p_bas       Battery Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_bas_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_sas_init(ble_sas_t * p_sas);

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the Battery Service.
 *
 * @note For the requirements in the BAS specification to be fulfilled,
 *       ble_lbs_battery_level_update() must be called upon reconnection if the
 *       battery level has changed while the service has been disconnected from a bonded
 *       client.
 *
 * @param[in]   p_bas      Battery Service structure.
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 */

uint32_t ble_sports_app_send_MMG(ble_sas_t * sasv2, unsigned char * value);

#endif // BLE_SAS_H__

/** @} */
